

# This is an example project that shows how to use Review Apps

This is a very basic project that has one static page, but it shows how to use
the environments feature of GitLab and the recently introduced dynamic
environments which can be used for Review Apps.

Review Apps allow you to create a new environment for each of your branches.
This review app is then visible as a link when you visit the merge request
for the branch. That way you are able to see all changes
introduced by the merge request changes, running live.

The example here uses a set of 3 environments:

1. `production`: you trigger deploys to production manually, by clicking the **Production**
   action under the **Pipelines** tab of your project.
1. `staging`: staging is deployed automatically when changes to `master` get
   merged.
1. `review/*`: the review app is created for any other branch that is pushed to GitLab.

See https://about.gitlab.com/2016/11/22/introducing-review-apps/

## Access the example

This project can be accessed under these addresses:

1. `production`: http://production.localhost.dev:5000/
1. `staging`: http://staging.localhost.dev:5000/
1. `review` `http://<xxx>.localhost.dev:5000/`


## Use it for your projects

This is a very simple example, but you can adapt it for your needs and have a
page that is deployed dynamically.

To do that you have to follow these few simple steps.

 1. Install a [GitLab Runner](https://docs.gitlab.com/runner/#using-gitlab-runner)

    Use a `shell` executor and assign a few tags: `review-apps`, `deploy`.

    ```
    gitlab-ci-multi-runner register
    WARNING: Running in user-mode.
    WARNING: Use sudo for system-mode:
    WARNING: $ sudo gitlab-runner...

    Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
    http://localhost:3000/ci
    Please enter the gitlab-ci token for this runner:
    MgZK4yxW_KGxayT9AiGq
    Please enter the gitlab-ci description for this runner:
    [eric-macbook]: eric-macbook-shell
    Please enter the gitlab-ci tags for this runner (comma separated):
    review-apps,deploy
    Whether to run untagged builds [true/false]:
    [false]:
    Registering runner... succeeded                     runner=MgZK4yxW
    Please enter the executor: docker-ssh, shell, ssh, virtualbox, docker, docker+machine, docker-ssh+machine, kubernetes, parallels:
    shell
    Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
    ```

 1. Configure your server, See [`static-review-app-server`](https://gitlab.com/madlittlemods/static-review-app-server)

 1. Add a Secure Variable with your Domain

    Add a `APPS_DOMAIN` to Secure Variables with your domain, ex.: `localhost.dev`

 1. Modify `.gitlab-ci.yml`

   Replace references to `/Users/eric/Documents/gitlab/static-review-app-server/www/$CI_BUILD_REF_SLUG/` to your own checkout of the [`static-review-app-server`]](https://gitlab.com/madlittlemods/static-review-app-server)

You can now start pushing your changes and see them live!
